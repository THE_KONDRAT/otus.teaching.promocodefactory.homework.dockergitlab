FROM mcr.microsoft.com/dotnet/sdk:7.0 AS BUILD
COPY /src /app
WORKDIR /app/Otus.Teaching.PromoCodeFactory.WebHost/
RUN dotnet restore && \
dotnet publish -c Release -o /out

FROM mcr.microsoft.com/dotnet/aspnet:7.0 AS RUNTIME
WORKDIR /app
COPY --from=BUILD /out/ ./
EXPOSE 42000
ENTRYPOINT ["dotnet", "Otus.Teaching.PromoCodeFactory.WebHost.dll"]